# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the azwallpaper package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: azwallpaper\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-28 21:47+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: bingWallpaperDownloader.js:42
msgid "No user defined directory found for BING wallpaper downloads"
msgstr ""

#: bingWallpaperDownloader.js:43 bingWallpaperDownloader.js:51
#, javascript-format
msgid "Directory set to - %s"
msgstr ""

#: bingWallpaperDownloader.js:50
msgid "Failed to create user defined directory for BING wallpaper downloads"
msgstr ""

#: bingWallpaperDownloader.js:140
msgid "BING wallpaper download failed."
msgstr ""

#: bingWallpaperDownloader.js:140
#, javascript-format
msgid "Error: %s"
msgstr ""

#: bingWallpaperDownloader.js:140
msgid "JSON data null"
msgstr ""

#: bingWallpaperDownloader.js:141
msgid "Try again?"
msgstr ""

#: extension.js:101
msgid "Next Wallpaper"
msgstr ""

#: extension.js:108
msgid "Slideshow Settings"
msgstr ""

#: extension.js:114
msgid "Display Settings"
msgstr ""

#: extension.js:115 prefs.js:35
msgid "Settings"
msgstr ""

#: prefs.js:43
msgid "Slideshow Options"
msgstr ""

#: prefs.js:48
msgid "Slideshow Directory"
msgstr ""

#: prefs.js:58
msgid "Slide Duration"
msgstr ""

#: prefs.js:59
msgid "Hours"
msgstr ""

#: prefs.js:59
msgid "Minutes"
msgstr ""

#: prefs.js:59
msgid "Seconds"
msgstr ""

#: prefs.js:125
msgid "Download BING wallpaper of the day"
msgstr ""

#: prefs.js:131
msgid "Download Directory"
msgstr ""

#: prefs.js:141
msgid "Notify on Download Error"
msgstr ""

#: prefs.js:142
msgid ""
"Displays a notification with error message and option to retry download."
msgstr ""

#: prefs.js:158
msgid "Image Resolution"
msgstr ""

#. TRANSLATORS: Markets are specific regions, defined by their language codes.
#. See https://learn.microsoft.com/en-us/bing/search-apis/bing-image-search/reference/market-codes
#: prefs.js:185
msgid "Market Location"
msgstr ""

#. TRANSLATORS: Markets are specific regions, defined by their language codes.
#. See https://learn.microsoft.com/en-us/bing/search-apis/bing-image-search/reference/market-codes
#: prefs.js:188
msgid ""
"The market where the BING wallpaper comes from. Wallpapers may vary in "
"different markets."
msgstr ""

#: prefs.js:219
msgid "Images to Download"
msgstr ""

#: prefs.js:220
msgid ""
"You can download up to 7 previous wallpapers plus the current wallpaper of "
"the day."
msgstr ""

#: prefs.js:227
msgid "Delete Previously Downloaded Wallpapers"
msgstr ""

#: prefs.js:239 prefs.js:266
#, javascript-format
msgid "Wallpaper files will be deleted after %s day"
msgid_plural "Wallpaper files will be deleted after %s days"
msgstr[0] ""
msgstr[1] ""

#: prefs.js:272
msgid "Debug Mode"
msgstr ""

#: prefs.js:284
msgid "Enable Logs"
msgstr ""

#: prefs.js:296
msgid "Open directory..."
msgstr ""

#: prefs.js:310
msgid "Choose new directory..."
msgstr ""

#: prefs.js:316
msgid "Select a directory"
msgstr ""

#: prefs.js:362
msgid "About"
msgstr ""

#: prefs.js:386
msgid "Wallpaper Slideshow"
msgstr ""

#: prefs.js:403
msgid "Wallpaper Slideshow Version"
msgstr ""

#: prefs.js:413
msgid "Git Commit"
msgstr ""

#: prefs.js:423
msgid "GNOME Version"
msgstr ""

#: prefs.js:432
msgid "OS Name"
msgstr ""

#: prefs.js:445
msgid "Windowing System"
msgstr ""

#: prefs.js:453
msgid "Wallpaper Slideshow GitLab"
msgstr ""

#: prefs.js:456
msgid "Donate via PayPal"
msgstr ""

#: prefs.js:465
msgid "Wallpaper Slideshow Settings"
msgstr ""

#: prefs.js:468
msgid "Load"
msgstr ""

#: prefs.js:473
msgid "Load Settings"
msgstr ""

#: prefs.js:501
msgid "Save"
msgstr ""

#: prefs.js:506
msgid "Save Settings"
msgstr ""

#: slideshow.js:23
msgid "Slideshow directory not found"
msgstr ""

#: slideshow.js:23 slideshow.js:112
msgid "Change directory in settings to begin slideshow"
msgstr ""

#: slideshow.js:24 slideshow.js:113
msgid "Open Settings"
msgstr ""

#: slideshow.js:112
msgid "Slideshow contains no slides"
msgstr ""
